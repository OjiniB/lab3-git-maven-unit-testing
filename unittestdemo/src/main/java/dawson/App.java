/**
 * Ojini Barsoumian
 * 1840580
 */

package dawson;

public class App 
{
    public int oneMore(int x){
        return x+1; // when we trying to put intentional bug we put x
    }
    public int echo(int x){
        return x;
    }
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
