/**
 * OJini Barsoumian
 * 1840580
 */

package dawson;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

      App app = new App();
      
    @Test
    public void testEcho_Success(){
        assertEquals("Error: they are not equal,'echo method'",5,app.echo(5));
    }

    @Test
    public void testOneMore_Fail(){
        assertEquals("Error: One more method", 5+1, app.oneMore(5), 0);

    }
}
